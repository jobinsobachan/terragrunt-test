variable "instance_name" {
    type = string
}

variable "instance_type" {
    type = string
}

variable "bucket_name" {
    type = string
}