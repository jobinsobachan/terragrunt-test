module "ec2" {
    source = "../../resource_modules/ec2"
    name = var.instance_name
    instance_type = var.instance_type
}