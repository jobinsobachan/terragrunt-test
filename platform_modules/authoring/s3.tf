module "s3" {
    source = "../../resource_modules/s3/object"
    bucket = var.bucket_name
    acl = "private"
    control_object_ownership = true
    object_ownership = "ObjectWriter"
    versioning = {
        enabled = true
    }
}